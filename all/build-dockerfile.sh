#!/bin/bash

# exit on error
set -e

# configuration
export NAMESPACE="${NAMESPACE:-byuhbll}"

# change to project directory
dir="$( cd "$( dirname "$0" )" && pwd )"
cd "$dir"

# create new Dockerfile
echo "FROM $NAMESPACE/payara" > Dockerfile.build

# add individuals to all-in-one
for image in zookeeper kafka solr mariadb mongo lamp
do 
  rsync -a --exclude=tags ../$image/ ./
  sed -i 's|^FROM.*||' Dockerfile
  sed -i 's|^CMD.*||' Dockerfile
  sed -i 's|^ENV DEFAULT_SERVICES.*||' Dockerfile
  cat Dockerfile >> Dockerfile.build
done

echo "ENV DEFAULT_SERVICES=\"zookeeper solr kafka payara mongo mariadb apache hang\"" >> Dockerfile.build

echo "CMD [\"/bin/bash\"]" >> Dockerfile.build

cp Dockerfile.build Dockerfile
