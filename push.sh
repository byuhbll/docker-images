#!/bin/bash

# exit on error
set -e

# configuration
export NAMESPACE="${NAMESPACE:-byuhbll}"

# change to project directory
dir="$( cd "$( dirname "$0" )" && pwd )"
cd "$dir"

# define images to push
if [ $# -eq 0 ]
then
  export images=( base java maven payara zookeeper kafka solr mariadb mongo lamp all )
else
  export images=( "$@" )
fi

# build each image
for image in "${images[@]}"
do
  # push latest image
  docker push $NAMESPACE/$image
  
  # push tags
  while read tag
  do
    docker push $NAMESPACE/$image:$tag
  done < $image/tags 
done
