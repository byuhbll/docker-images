#!/bin/bash

# exit on error
set -e

# get action
export action=$(basename $0) 

# determine services to start
if [ $# -eq 0 ]; then
  export services="$DEFAULT_SERVICES"
else
  export services="$@"
fi

# Start all specified services
for service in $services
do
  if [ $service == "hang" ]
  then
    echo "$action complete, hanging (ctrl+c to return)"
    
    # signal that services are ready
    touch /tmp/ready
    
    # hang until ctrl+c 
    tail -f /dev/null
  else
    systemctl $action $service
  fi
done
