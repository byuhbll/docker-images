#!/bin/bash

# exit on error
set -e

# use first argument as timeout in seconds; use default value if not supplied
TIMEOUT=${1:-600}

# function that loops until ready file is present checking every second
checkReady () {
  while [ ! -f /tmp/ready ]; do sleep 1; done
}

# export the checkReady function so child processes can run it
export -f checkReady

# run the checkReady function with a timeout
timeout --foreground $TIMEOUT bash -c checkReady
