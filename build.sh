#!/bin/bash

# exit on error
set -e

# pull latest centos 7 image
docker pull centos:7

# configuration
export NAMESPACE="${NAMESPACE:-byuhbll}"

# change to project directory
dir="$( cd "$( dirname "$0" )" && pwd )"
cd "$dir"

# define images to build (order is important)
if [ $# -eq 0 ]
then
  export images=( base java maven payara zookeeper kafka solr mariadb mongo lamp all )
else
  export images=( "$@" )
fi

# build each image
for image in "${images[@]}"
do
  if [ "$image" == "all" ]
  then
    $image/build-dockerfile.sh
  fi
  
  # build the image  
  docker build -t $NAMESPACE/$image $image
  
  # tag
  while read tag
  do
    docker tag $NAMESPACE/$image $NAMESPACE/$image:$tag
  done < $image/tags
done
