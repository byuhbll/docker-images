General purpose Docker images representing the environment of the HBLL. These images are designed for use in prototyping, development and testing.

The `build.sh` script will build all images in proper order. To build a specific image, provide the image name(s) as an argument to the script (ie, `build.sh solr`). Be sure that any dependent images are already built and available.

There is a `start`, `stop`, and `restart` command inside each image. These are convenience commands for starting, stopping, or restarting services. Examples:

* `start mongo` starts mongodb.
* `start zookeeper kafka` starts zookeeper and kafka in that order.
* `start` starts all default services for the image.
* replace `start` with `stop` or `restart` to stop or restart services respectively.

The all-in-one has all services available in one image. This is convenient for accessing any of the services on localhost and without authentication. Services available in the all-in-one include:

* payara
* zookeeper
* kafka
* solr
* mariadb
* mongo
